@extends('layouts.admin')

@section('content')
<!-- ======= Gallery Section ======= -->
<br><br><br>
<section id="gallery" class="gallery">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Gallery</h2>
          <p>Cek Galeri Kami</p>
        </div>

        <div class="row g-0" data-aos="fade-left">

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="100">
              <a href="{{asset('template/assets/img/bikang8.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/bikang8.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="150">
              <a href="{{asset('template/assets/img/pastel.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/pastel.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="200">
              <a href="{{asset('template/assets/img/tok.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/tok.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="250">
              <a href="{{asset('template/assets/img/onde.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/onde.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="300">
              <a href="{{asset('template/assets/img/nasi4.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/nasi4.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="350">
              <a href="{{asset('template/assets/img/ultah.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/ultah.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="400">
              <a href="{{asset('template/assets/img/puding.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/puding.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/nasi3.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/nasi3.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/kukus3.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/kukus3.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/apem.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/apem.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/rainbow2.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/rainbow2.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/rainbow3.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/rainbow3.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/bikang4.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/bikang4.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/onde5.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/onde5.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/kukus.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/kukus.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/kukus4.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/kukus4.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/pesanan2.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/pesanan2.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/pengantin.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/pengantin.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/pesanan.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/pesanan.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
              <a href="{{asset('template/assets/img/bolu.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/bolu.jpg')}}" alt="" class="img-fluid">
              </a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Gallery Section -->
    @endsection