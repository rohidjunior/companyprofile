@extends('layouts.admin')

@section('content')
<!-- ======= Product Section ======= -->
<br><br><br>
<section id="product" class="features">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Product</h2>
          <p>Cek Produk Kami</p>
        </div>

        <div class="row" data-aos="fade-left">
          <div class="col-lg-3 col-md-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="50">
              <i class="" style="color: #ffbb2c;"></i>
              <h3><a href="{{asset('template/assets/img/bikang8.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/bikang8.jpg')}}" alt="" class="img-fluid">Kue Bikang</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="100">
              <i class="" style="color: #5578ff;"></i>
              <h3><a href="{{asset('template/assets/img/onde.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/onde.jpg')}}" alt="" class="img-fluid">Onde-Onde</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="150">
              <i class="" style="color: #e80368;"></i>
              <h3><a href="{{asset('template/assets/img/tok.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/tok.jpg')}}" alt="" class="img-fluid">Kue Tok</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4 mt-lg-0">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="200">
              <i class="" style="color: #e361ff;"></i>
              <h3><a href="{{asset('template/assets/img/pastel5.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/pastel5.jpg')}}" alt="" class="img-fluid">Kue Pastel</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="250">
              <i class="" style="color: #47aeff;"></i>
              <h3><a href="{{asset('template/assets/img/bolu.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/bolu.jpg')}}" alt="" class="img-fluid">Kue Bolu</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="300">
              <i class="" style="color: #ffa76e;"></i>
              <h3><a href="{{asset('template/assets/img/ultah.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/ultah.jpg')}}" alt="" class="img-fluid">Kue Ulang Tahun</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="350">
              <i class="" style="color: #11dbcf;"></i>
              <h3><a href="{{asset('template/assets/img/puding.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/puding.jpg')}}" alt="" class="img-fluid">Puding</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="400">
              <i class="" style="color: #4233ff;"></i>
              <h3><a href="{{asset('template/assets/img/kukus4.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/kukus4.jpg')}}" alt="" class="img-fluid">Kue Bolu Kukus</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="450">
              <i class="" style="color: #b2904f;"></i>
              <h3><a href="{{asset('template/assets/img/apem.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/apem.jpg')}}" alt="" class="img-fluid">Kue Apem</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="500">
              <i class="" style="color: #b20969;"></i>
              <h3><a href="{{asset('template/assets/img/rainbow2.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/rainbow2.jpg')}}" alt="" class="img-fluid">Rainbow Cake</a></h3>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box" data-aos="zoom-in" data-aos-delay="550">
              <i class="" style="color: #ff5828;"></i>
              <h3><a href="{{asset('template/assets/img/nasi5.jpg')}}" class="gallery-lightbox">
                <img src="{{asset('template/assets/img/nasi5.jpg')}}" alt="" class="img-fluid">Nasi Kotak</a></h3>
            </div>
          </div>
          </div>
        </div>

      </div>
    </section><!-- End Product Section -->
    @endsection