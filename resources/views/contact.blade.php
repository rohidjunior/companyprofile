@extends('layouts.admin')

@section('content')
<!-- ======= Contact Section ======= -->
<br><br><br>
<section id="contact" class="contact">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Contact Us</h2>
          <p>Kontak Kami</p>
        </div>

        <div class="row">

          <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
            <div class="info">
              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h4>Lokasi:</h4>
                <p>Dsn. Prayungan RT/RW 001/002, Ds. Kuwik, Kec. Kunjang, Kab. Kediri</p>
              </div>

              <div class="email">
                <i class="bi bi-envelope"></i>
                <h4>Email:</h4>
                <p>rohidjunior@gmail.com</p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Telp:</h4>
                <p>0822 3187 5579</p>
              </div>

            </div>

          </div>

          <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left" data-aos-delay="200">

              <div class="text-center">
                <h4>Order Melalui WhatsApp Disini</h4>
                <a href="https://api.whatsapp.com/send?phone=6282231875579" class="btn btn-success">Kirim Pesan</a></div>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->
    @endsection